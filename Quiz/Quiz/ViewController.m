//
//  ViewController.m
//  Quiz
//
//  Created by Sathish Jayapal on 11/18/14.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.questions=@[ @"From what is congnac made of", @"Seventh wonder of the world is", @"Captiol of WI"];
    self.answers=@[@"grapes", @"Taj Mahal", @"Madison"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)showQuestion:(id)sender{
    self.currentQuestionIndex++;
    if (self.currentQuestionIndex==self.questions.count) {
        self.currentQuestionIndex=0;
    }
    NSString *question= self.questions[self.currentQuestionIndex];
    self.questionLabel.text= question;
    self.answerLabel.text=@"??????";
    
    
}
-(IBAction)showAnswer:(id)sender{
    NSString *answer= self.answers[self.currentQuestionIndex];
    self.answerLabel.text=answer;
    
}

@end
