//
//  ViewController.h
//  Quiz
//
//  Created by Sathish Jayapal on 11/18/14.
//
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic) int currentQuestionIndex;
@property (nonatomic,weak) IBOutlet UILabel *questionLabel;
@property (nonatomic,weak) IBOutlet UILabel *answerLabel;
@property (nonatomic,strong) NSArray *questions;
@property (nonatomic,strong) NSArray *answers;

@end

